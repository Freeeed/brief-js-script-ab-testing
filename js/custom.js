function new_version() {
    const changeimage = document.querySelector('.header-logo img'); // je crée une const que j'appelle "changeimage" elle va chercher la balise img dans la class header-logo
    const new_image = "img/logo-default.png" // je crée une const "new_image" qui contient le src de la nouvelle image
    changeimage.setAttribute("src", new_image); // je dis a ma const "changeimage" de changer le src de sa balise img grace au setAttribute, par le src contenu dans ma const "newimage"

    const product_img = document.querySelector('.img-fluid'); // je crée une const que j'appelle "product_img" elle va chercher la class '.img_fluid'
    const new_product = "img/products/sac.png" // je crée une const "new_product" qui contient le src de la nouvelle image
    product_img.setAttribute("src", new_product); // je dis a ma const "product_img" de changer le src grace au setAttribute, par le src contenu dans ma const "new_product"

    const remove_sidebar = document.querySelector('.container > .row > .col-lg-3'); // je crée une const "remove_sidebar" qui va cibler la class "col-lg-3" en passant par tout ses parents
    remove_sidebar.remove(); // je dis a ma const de delete la class "col-lg-3" grace au .remove()

    const change_classname = document.querySelector('.main > .container > .row > .col-lg-9'); // je crée une const "change_classname" qui va cibler la class "col-lg-9" en passant par tout ses parents
    const new_class_name = "col-lg-12" // je crée une constante "new_image" qui contient le nouveau nom de class (ça va changer la taille vu que c'est du bootstrap)
    change_classname.setAttribute("class", new_class_name); // je dis a ma const "change_classname" de changer la class grace au setAttribute, par la class contenu dans ma const "new_class_name" 

    const related_prod_img1 = document.querySelector('.masonry-loader > .row > div:nth-child(1) > span a:nth-child(2) img'); // je crée une const qui va selectioner l'image que je veux changer. :nth-child() permet de selectionner l'enfant souhaité quand il y en plusieur et que rien les differencis
    const new_related_prod_img1 = "img/products/appphoto.png" // je crée une const qui contient le src de la nouvelle image
    related_prod_img1.setAttribute("src", new_related_prod_img1); // je dis a ma const de modifier le src par celui de l'autre const

    const related_prod_img2 = document.querySelector('.masonry-loader > .row > div:nth-child(2) > span a:nth-child(2) img');
    const new_related_prod_img2 = "img/products/golf.png"
    related_prod_img2.setAttribute("src", new_related_prod_img2);

    const related_prod_img3 = document.querySelector('.masonry-loader > .row > div:nth-child(3) > span a:nth-child(2) img');
    const new_related_prod_img3 = "img/products/shoes.png"
    related_prod_img3.setAttribute("src", new_related_prod_img3);

    const related_prod_img4 = document.querySelector('.masonry-loader > .row > div:nth-child(4) > span a:nth-child(2) img');
    const new_related_prod_img4 = "img/products/bag.png"
    related_prod_img4.setAttribute("src", new_related_prod_img4);

    const new_sub_btn = document.querySelector('.input-group-append button'); // je crée une const qui va select l'element html souhaité
    new_sub_btn.style.borderRadius = "0px"; // je dis a ma const de modifier le border radius de l'élement quelle selectione. syntax = const name.style.la property css a changer (vérif orthographe de la syntax online)

    const div_cat = document.querySelector('.product-meta'); // je crée une const qui va select la class 'product-meta' dans l'html
    const div_switch = div_cat.parentNode // je crée une const qui grace au .parentNode me permet d'aller ciblé le parent de 'product-meta'
    const div_addto = document.querySelector('.cart'); // je crée une const qui va select la class 'cart'
    div_switch.insertBefore(div_cat, div_addto); // je prend donc ma const 'parent' et avec le .insertBefore je lui dit de mettre 'div_cat' avant 'div_addto'

    const summary_flex = document.querySelector('.cart');
    summary_flex.style.display = "flex";
    summary_flex.style.flexDirection = "column";
    summary_flex.style.alignItems = "center";
    summary_flex.style.height = "225px";
    summary_flex.style.justifyContent = "flex-end";

    const btn_size = document.querySelector('.cart > button');
    btn_size.style.height = "65px"
    btn_size.style.width = "165px"

    const switch_btn = document.getElementById('mainNav');
    const add_btn = document.createElement('li');
    add_btn.appendChild(document.createTextNode("New Version"));
    add_btn.classList.add("test");
    switch_btn.appendChild(add_btn);

    const add_a_to_li = document.querySelector('.collapse > .nav > .test')
    console.log(add_a_to_li);






}
new_version();